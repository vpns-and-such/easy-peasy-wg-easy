# EASY-PEASY-WG-EASY

A simple WireGuard setup based entirely on [WireGuard Easy][wgeasy].

## Prerequisites

- A host with a kernel that supports WireGuard (e.g. `Ubuntu 20.04`)
- `sudo` permissions
- `bash`
- `git`
- Docker Engine
- Docker Compose

Here's a cheetsheet for `Ubuntu 20.04` in case you're lazy:

```bash
# Update packages list
sudo apt-get -y update

# Install deps
sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Add Docker's signing key
curl -fsSL 'https://download.docker.com/linux/ubuntu/gpg' | sudo gpg \
    --dearmor -o '/usr/share/keyrings/docker-archive-keyring.gpg'

# Add Docker's repo
echo "deb [arch=$(dpkg --print-architecture) \
    signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee \
    '/etc/apt/sources.list.d/docker.list' > /dev/null

# Install Docker
sudo apt-get -y update \
    && sudo apt-get -y install \
        docker-ce
        docker-ce-cli
        containerd.io

# Install Docker Compose
sudo curl -L \
    "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o '/usr/local/bin/docker-compose' \
    && sudo chmod +x '/usr/local/bin/docker-compose'

# Grant your user perissions to execute Docker
sudo usermod -aG docker $(whoami)
```

## Recommendation

It is recommended to run WireGuard behind a reverse proxy. An example config file for `NGINX` is available [here][nginx_example]

It is recommended to run the UI with TLS enabled, you can use [Let's Encrypt][letsencrypt] and [certbot][certbot_nginx_docs] to achieve that easily.

## Settings

Read the docs for [WireGuard Easy][wgeasy] project for more informartion and further customization.

As for this projects, all of its settings are supplied via environment variables. Here's the list:

- `WG_EASY_PWD` – plaintext string containing password to the UI
- `WG_UI_LOCALHOST` – if set to `false` will portmap the UI to `0.0.0.0:51821`, otherwise maps the UI to `127.0.0.1:51821`

## How2Run

- Export `WG_EASY_PWD` variable to your environment
- Clone this dir on your target server
- Change your workdir to this dir (do the `cd $(pwd)/easy-peasy-wg-easy`)
- Run the launch script (do the `./run.sh` thing)

Here's a cheetsheet in case you're lazy:

```bash
export WG_EASY_PWD='<YOUR_SUPER_SECURE_PASSWORD_HERE>'

cd "${HOME}" \
    && git clone https://gitlab.com/vpns-and-such/easy-peasy-wg-easy \
    && cd easy-peasy-wg-easy \
    && ./run.sh
```

[wgeasy]: https://github.com/WeeJeWel/wg-easy
[nginx_example]: ./nginx_wg.conf
[letsencrypt]: https://letsencrypt.org/getting-started/
[certbot_docs]: https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal
