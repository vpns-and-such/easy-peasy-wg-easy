#!/bin/bash -l
set +x

# Base settings
WG_DATA_DIR=".wg-easy"

# Simple logger
CLR='\033[0;32m'
NC='\033[0m'
log() {
    echo -e "${CLR}${1}${NC}"
}

host_setter() {
    local host_ip
    local portmap
    portmap='127.0.0.1:51821:51821'
    host_ip=$(curl -s 'https://api.ipify.org')
    if [[ ${WG_UI_LOCALHOST} == "false" ]]; then
        portmap='51821:51821'
    fi
    sed -i "s,<HOST_PLACEHOLDER>,${host_ip},g" docker-compose.yaml
    sed -i "s,<PORTMAP_PLACEHOLDER>,${portmap},g" docker-compose.yaml
    sed -i "s,<DATADIR_PLACEHOLDER>,${WG_DATA_DIR},g" docker-compose.yaml
}

# Start OpenVPN server
log "Launching WireGuard server..."
if [ ! -f "${WG_DATA_DIR}" ]; then
    log "First run detected, running prep steps..."
    host_setter
    docker-compose up -d
else
    docker-compose up -d
fi

# Output client config
log "Done!"
